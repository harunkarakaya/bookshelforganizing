﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookShelfOrganizing.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] shelfLength = new int[2,5] { { 150, 120 , 300, 180, 110 },
                                                {0,0,0,0,0} } ;
            string[,] books = new string[2, 5] { { "AGameofThrones", "DancingWithTheStarman", "RocketLaunchers", "DontPanic!", "AnElectricCarAnd400MKilometers" },
                                                  { "118","170","110","160","310"}}; //book name and book page count

            List<int> result = BookShelfOrganizing(shelfLength, books);

            ShelfsWrite(shelfLength);
            BooksWrite(books);
            StatusofShelves(shelfLength);

            string message = (result.Count == 0) ? "Kitaplar raflara sığıyor. Raf satın alınabilir." : "Kitaplar raflara sığmıyor. Raf satın alınamaz.";
            Console.WriteLine("\n\n" + message);
            Console.Write("Sığmayan kitap sayfaları:");

            for (int i = 0; i < result.Count; i++)
            {
                Console.Write(result[i].ToString() + ",");
            }

            Console.ReadLine();
        }

        static public List<int> BookShelfOrganizing(int[,] shelfLength, string[,] books)
        {
            List<int> bookList = new List<int>();

            int SmallNumberShelf = 0, SmallNumberShelfIndex = 0;
            int SmallNumberBook = 0, SmallNumberBookIndex = 0;
            int SmallShelfCount = 0, SmallBookCount = 0;
            int FullShelf = 0;
            int NumberOfBooks = books.Length / 2;

            for (int k = 0; k < NumberOfBooks; k++)
            {
                for (int i = 0; i < NumberOfBooks; i++)
                {
                    for (int j = 0; j < NumberOfBooks; j++)
                    {
                        if (shelfLength[0, i] < shelfLength[0, j])
                        {
                            SmallShelfCount++;
                        }

                        if (Int32.Parse(books[1, i]) < Int32.Parse(books[1, j]))
                        {
                            SmallBookCount++;
                        }
                    }


                    if (SmallShelfCount == NumberOfBooks - 1 - FullShelf)
                    {
                        SmallNumberShelf = shelfLength[0, i]; //Dögüdeki En küçük rafın sayıca büyüklüğü
                        SmallNumberShelfIndex = i; //Döngüdeki En küçük rafın index'i

                    }

                    if (SmallBookCount == NumberOfBooks - 1 - FullShelf)
                    {
                        SmallNumberBook = Int32.Parse(books[1, i]);
                        SmallNumberBookIndex = i;

                    }

                    SmallShelfCount = 0;
                    SmallBookCount = 0;
                }

                

                if (SmallNumberBook <= SmallNumberShelf) // En küçük kitap herhangi bir rafa sığmıyor ise o kitaplık uygun değildir.Satınalma için Her kitabın sığması gereklidir.
                {
                    shelfLength[1, SmallNumberShelfIndex] = 1; //Kitap koyuldu.
                    FullShelf++;
                }
                else//0 boş raf, 1 doldurulan raf, -1 sığmayıp boş kalan raf
                {
                    shelfLength[1, SmallNumberShelfIndex] = -1; //Kitap koyulamadı.
                    FullShelf++;
                    bookList.Add(SmallNumberBook);
                }

            }

            for (int i = 0; i < NumberOfBooks; i++)
            {
                if(shelfLength[1,i] == -1)
                {
                    return bookList;
                }
                
            }

            return bookList;
        }

        static public void ShelfsWrite(int[,] shelfLength)
        {
            Console.Write("Rafların uzunluğu(mm):");
            for (int i = 0; i < shelfLength.Length / 2; i++)
            {
                Console.Write(shelfLength[0, i] + ",");
            }
        }

        static public void BooksWrite(string[,] books)
        {

            Console.Write("\nKitapların uzunluğu(mm):");
            for (int i = 0; i < books.Length / 2; i++)
            {
                Console.Write(books[1, i] + ",");
            }
        }

        static public void StatusofShelves(int[,] shelfLength)
        {

            Console.Write("\nRafların durumu:");
            for (int i = 0; i < shelfLength.Length / 2; i++)
            {
                if (shelfLength[1, i] == 1)
                {
                    Console.Write("Dolu" + ",");
                }
                if (shelfLength[1, i] == -1)
                {
                    Console.Write("Boş" + ",");
                }
            }
        }
    }

}
